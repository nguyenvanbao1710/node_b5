CREATE TABLE food_type (
  type_id int NOT NULL AUTO_INCREMENT,
  type_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (type_id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT ;

CREATE TABLE like_res (
  user_id int NOT NULL,
  res_id int NOT NULL,
  date_like datetime DEFAULT NULL,
  PRIMARY KEY (user_id,`res_id`),
  KEY res_id (res_id),
  CONSTRAINT like_res_ibfk_1 FOREIGN KEY (user_id) REFERENCES user (user_id),
  CONSTRAINT like_res_ibfk_2 FOREIGN KEY (res_id) REFERENCES restaurant (res_id)
) ENGINE=InnoDB DEFAULT ;

CREATE TABLE order (
  user_id int NOT NULL,
  food_id int NOT NULL,
  amount int DEFAULT NULL,
  code varchar(255) DEFAULT NULL,
  arr_sub_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (user_id,`food_id`),
  KEY food_id (food_id),
  CONSTRAINT order_ibfk_1 FOREIGN KEY (user_id) REFERENCES user (user_id),
  CONSTRAINT order_ibfk_2 FOREIGN KEY (food_id) REFERENCES food (food_id)
) ENGINE=InnoDB DEFAULT ;

CREATE TABLE rate_res (
  user_id int NOT NULL,
  res_id int NOT NULL,
  amount int DEFAULT NULL,
  date_rate datetime DEFAULT NULL,
  PRIMARY KEY (user_id,`res_id`),
  KEY res_id (res_id),
  CONSTRAINT rate_res_ibfk_1 FOREIGN KEY (user_id) REFERENCES user (user_id),
  CONSTRAINT rate_res_ibfk_2 FOREIGN KEY (res_id) REFERENCES restaurant (res_id)
) ENGINE=InnoDB DEFAULT 
CREATE TABLE restaurant (
  res_id int NOT NULL AUTO_INCREMENT,
  res_name varchar(255) DEFAULT NULL,
  image varchar(255) DEFAULT NULL,
  desc varchar(255) DEFAULT NULL,
  PRIMARY KEY (res_id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT ;

CREATE TABLE sub_food (
  sub_id int NOT NULL AUTO_INCREMENT,
  sub_name varchar(255) DEFAULT NULL,
  sub_price int DEFAULT NULL,
  food_id int DEFAULT NULL,
  PRIMARY KEY (sub_id),
  KEY food_id (food_id),
  CONSTRAINT sub_food_ibfk_1 FOREIGN KEY (food_id) REFERENCES food (food_id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT ;

CREATE TABLE user (
  user_id int NOT NULL AUTO_INCREMENT,
  full_name varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  pass_word varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (user_id)
)